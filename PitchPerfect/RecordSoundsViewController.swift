//
//  RecordSoundsViewController.swift
//  PitchPerfect
//
//  Created by Gabriel Morris on 5/17/17.
//  Copyright © 2017 Gabriel. All rights reserved.
//

import UIKit
import AVFoundation

class RecordSoundsViewController: UIViewController, AVAudioRecorderDelegate {
	
	var audioRecorder: AVAudioRecorder!
	
	//MARK: Outlets
	@IBOutlet weak var recordingLabel: UILabel!
	@IBOutlet weak var recordButton: UIButton!
	@IBOutlet weak var stopRecordingButton: UIButton!
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		stopRecordingButton.isEnabled = false
	}
	
	//Prepare for segue to PlaySoundsViewController
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "stopRecording" {
			let playSoundsVC = segue.destination as! PlaySoundsViewController
			let recordedAudioURL = sender as! URL
			playSoundsVC.recordedAudioURL = recordedAudioURL
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	//MARK: Actions
	@IBAction func recordAudio(_ sender: AnyObject) {
		configureUI(isRecording: true)
		
		//AVFoundation
		let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0] as String
		let recordingName = "recordedVoice.wav"
		let pathArray = [dirPath, recordingName]
		let filePath = URL(string: pathArray.joined(separator: "/"))
		print(filePath!)
		
		let session = AVAudioSession.sharedInstance()
		try! session.setCategory(AVAudioSessionCategoryPlayAndRecord, with:AVAudioSessionCategoryOptions.defaultToSpeaker)
		
		try! audioRecorder = AVAudioRecorder(url: filePath!, settings: [:])
		audioRecorder.delegate = self
		audioRecorder.isMeteringEnabled = true
		audioRecorder.prepareToRecord()
		audioRecorder.record()
	}
	@IBAction func stopRecording(_ sender: AnyObject) {
		configureUI(isRecording: false)
		audioRecorder.stop()
		let audioSession = AVAudioSession.sharedInstance()
		try! audioSession.setActive(false)
	}
	
	func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
		if flag {
			print("Finished recording")
			performSegue(withIdentifier: "stopRecording", sender: audioRecorder.url)
		} else {
			print("The recording failed")
		}
	}
	func configureUI(isRecording: Bool) {
		if isRecording == true {
			recordingLabel.text = "Recording in progress"
			recordButton.isEnabled = false
			stopRecordingButton.isEnabled = true
		} else {
			recordingLabel.text = "Tap to record"
			stopRecordingButton.isEnabled = false
			recordButton.isEnabled = true
		}
	}
}

